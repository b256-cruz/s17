/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



const userData = function() {
    let fullName = prompt("What is your name?")
    let age = prompt("What is your age?")
    let location = prompt("Where do you live?")

    console.log("Hello," + " " +fullName)
    console.log("You are" + " " + age + " " + "years old.")
    console.log("You live in" + " " + location)
}

userData()

const favoriteBand = function(){
    alert("Name your top five bands/musical artists")
    let firstBand = prompt("Name your top five bands/musical artists - 1")
    let secondtBand = prompt("Name your top five bands/musical artists - 2")
    let thirdBand = prompt("Name your top five bands/musical artists - 3")
    let fourthBand = prompt("Name your top five bands/musical artists - 4")
    let fifthBand = prompt("Name your top five bands/musical artists - 5")

    console.log("1." + " " + firstBand)
    console.log("2." + " " + secondtBand)
    console.log("3." + " " + thirdBand)
    console.log("4." + " " + fourthBand)
    console.log("5." + " " + fifthBand)
}

favoriteBand()

const favoriteMovies = function(){
    alert("Name your top five favorite movies")
    let firstMovie = prompt("Name your top five favorite movies - 1")
    let firstMovieRating = prompt(`Please Search for Rotten Tomatoes Rating of ${firstMovie}`)
    let secondMovie = prompt("Name your top five favorite movies - 2")
    let secondMovieRating = prompt(`Please Search for Rotten Tomatoes Rating of ${secondMovie}`)
    let thirdMovie = prompt("Name your top five favorite movies - 3")
    let thirdMovieRating = prompt(`Please Search for Rotten Tomatoes Rating of ${thirdMovie}`)
    let fourthMovie = prompt("Name your top five favorite movies - 4")
    let fourthMovieRating = prompt(`Please Search for Rotten Tomatoes Rating of ${fourthMovie}`)
    let fifthMovie = prompt("Name your top five favorite movies - 5")
    let fifthMovieRating = prompt(`Please Search for Rotten Tomatoes Rating of ${fifthMovie}`)

    console.log("1." + " " + firstMovie)
    console.log("Rotten Tomatoes Rating" + " " + firstMovieRating +"%")
    console.log("1." + " " + secondMovie)
    console.log("Rotten Tomatoes Rating" + " " + secondMovieRating +"%")
    console.log("1." + " " + thirdMovie)
    console.log("Rotten Tomatoes Rating" + " " + thirdMovieRating +"%")
    console.log("1." + " " + fourthMovie)
    console.log("Rotten Tomatoes Rating" + " " + fourthMovieRating +"%")
    console.log("1." + " " + fifthMovie)
    console.log("Rotten Tomatoes Rating" + " " + fifthMovieRating +"%")
}

favoriteMovies()


console.log("-----------------------------------DEDUG SECTION-----------------------------------")
printUsers();

function printUsers()  {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);